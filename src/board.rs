use crate::piece::*;

use owo_colors::OwoColorize;
use owo_colors::Rgb;
use std::io::stdin;

#[derive(Clone, Copy)]
pub struct Board {
    pub squares: [[Square; 8]; 8],
}

pub struct Coord {
    pub x: usize,
    pub y: usize,
}

pub struct ChessMove(pub Coord, pub Coord);

impl Board {
    pub fn new() -> Board {
        let mut pieces = [[Square {
            color: None,
            piece: None,
        }; 8]; 8];

        for i in 0..8 {
            pieces[1][i] = Square {
                color: Some(Color::White),
                piece: Some(Type::Pawn),
            };

            pieces[6][i] = Square {
                color: Some(Color::Black),
                piece: Some(Type::Pawn),
            };

            let piece_types = match i {
                7 | 0 => Type::Rook,
                6 | 1 => Type::Knight,
                5 | 2 => Type::Bishop,
                4     => Type::King,
                3     => Type::Queen,
                _     => unreachable!(),
            };

            pieces[7][i] = Square {
                color: Some(Color::Black),
                piece: Some(piece_types),
            };

            pieces[0][i] = Square {
                color: Some(Color::White),
                piece: Some(piece_types),
            };
        }

        Board { squares: pieces }
    }

    fn print(&self) {
        let mut bg_iterator: usize = 0;
        print!("{esc}[2J{esc}[1;1H", esc = 27 as char);

        for (i, row) in self.squares.iter().enumerate().rev() {
            bg_iterator += 1;
            for col in row.iter() {
                bg_iterator += 1;
                if bg_iterator % 2 == 1 {
                    print!("{}", col.on_color(Rgb(40, 40, 40)))
                } else {
                    print!("{}", col.on_color(Rgb(251, 241, 199)))
                }
            }
            println!(" {}", i + 1)
        }
        println!(" a  b  c  d  e  f  g  h")
    }

    pub fn start_game(&mut self) {
        let mut input = String::new();
        let mut chess_move: ChessMove;
        let mut warning = String::new();

        loop {
            self.print();
            println!(
                "{}",
                &warning.on_color(Rgb(255, 105, 97)).color(Rgb(8, 8, 8))
            );

            warning.clear();
            input.clear();

            stdin()
                .read_line(&mut input)
                .expect("Failed to read line {}");

            if input.chars().count() != 5 {
                warning = "Invalid input.".to_string();
                continue;
            }

            chess_move = Board::convert_move(&input);

            self.move_piece(chess_move);
        }
    }

    fn convert_move(input: &str) -> ChessMove {
        let convert_letter = |letter: &str| -> usize {
            match letter {
                "a" => 1,
                "b" => 2,
                "c" => 3,
                "d" => 4,
                "e" => 5,
                "f" => 6,
                "g" => 7,
                "h" => 8,
                _   => 9,
            }
        };

        ChessMove(
            Coord {
                x: convert_letter(&input[0..1]) - 1,
                y: input[1..2].parse::<usize>().unwrap() - 1,
            },
            Coord {
                x: convert_letter(&input[2..3]) - 1,
                y: input[3..4].parse::<usize>().unwrap() - 1,
            },
        )
    }

    fn move_piece(&mut self, chess_move: ChessMove) {
        self.squares[chess_move.1.y][chess_move.1.x] = self.squares[chess_move.0.y][chess_move.0.x];
        self.squares[chess_move.0.y][chess_move.0.x] = Square {
            color: None,
            piece: None,
        };
    }
}
