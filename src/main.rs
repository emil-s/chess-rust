pub(crate) mod board;
pub(crate) mod moveset;
pub(crate) mod piece;

use board::Board;

fn main() {
    let mut chess_board = Board::new();
    chess_board.start_game()
}
