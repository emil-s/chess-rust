use crate::board::{ChessMove, Coord, Board};
use crate::piece::*;

pub trait Moveset {
    fn check_move(&self, board: &[[Square; 8]; 8], chess_move: &ChessMove) -> bool;

    fn check_pawn(board: &[[Square; 8]; 8], chess_move: &ChessMove) -> bool {
        true
    }

    fn check_knight(board: &[[Square; 8]; 8], chess_move: &ChessMove) -> bool {
        true
    }

    fn check_biship(board: &[[Square; 8]; 8], chess_move: &ChessMove) -> bool {
        true
    }

    fn check_rook(board: &[[Square; 8]; 8], chess_move: &ChessMove) -> bool {
        true
    }

    fn check_queen(board: &[[Square; 8]; 8], chess_move: &ChessMove) -> bool {
        true
    }

    fn check_king(board: &[[Square; 8]; 8], chess_move: &ChessMove) -> bool {
        true
    }

    fn check_straight() -> bool {
        true
    }
}
