use crate::board::ChessMove;
use crate::moveset::Moveset;

use owo_colors::{OwoColorize, Rgb};
use std::fmt;

#[derive(Clone, Copy)]
pub enum Type {
    King,
    Queen,
    Rook,
    Bishop,
    Knight,
    Pawn,
}

#[derive(Clone, Copy)]
pub enum Color {
    White,
    Black,
}

#[derive(Clone, Copy)]
pub struct Square {
    pub piece: Option<Type>,
    pub color: Option<Color>,
}

impl Square {
    fn get_string(&self) -> &str {
        match self.piece {
            None               => "   ",
            Some(Type::Pawn)   => " ♟ ",
            Some(Type::Knight) => " ♞ ",
            Some(Type::Bishop) => " ♝ ",
            Some(Type::Rook)   => " ♜ ",
            Some(Type::Queen)  => " ♛ ",
            Some(Type::King)   => " ♚ ",
        }
    }

    fn get_color(&self) -> (u8, u8, u8) {
        match self.color {
            None => (0, 0, 0),
            Some(Color::White) => (69, 133, 136),
            Some(Color::Black) => (214, 93, 14),
        }
    }
}

impl Moveset for Square {
    fn check_move(&self, board: &[[Square; 8]; 8], chess_move: &ChessMove) -> bool {
        match self.piece {
            None               => false,
            Some(Type::Pawn)   => Self::check_pawn(&board, &chess_move),
            Some(Type::Knight) => Self::check_knight(&board, &chess_move),
            Some(Type::Bishop) => Self::check_biship(&board, &chess_move),
            Some(Type::Rook)   => Self::check_rook(&board, &chess_move),
            Some(Type::Queen)  => Self::check_queen(&board, &chess_move),
            Some(Type::King)   => Self::check_king(&board, &chess_move),
        }
    }
}

impl fmt::Display for Square {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let (r, g, b) = self.get_color();
        write!(f, "{}", self.get_string().color(Rgb(r, g, b)))
    }
}
